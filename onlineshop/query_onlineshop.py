# -*- coding: utf-8 -*-
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
			'onlineshop.settings')
import django
django.setup()
from shop.models import Category, Product
from django.core.files import File
from django.conf import settings


prodName='ofertas'
try:
    ofertas = Category.objects.get(catName=prodName)
    print("-----------"+prodName+" ya existe en la base de datos"+"-----------");

except Category.DoesNotExist:
	ofertas=Category.objects.get_or_create(catName=prodName)[0]
	ofertas.save()
	print("-----------"+prodName+" creadas correctamente"+"-----------");


prodName='gangas'
try:
    gangas = Category.objects.get(catName=prodName)
    print("-----------"+prodName+" ya existe en la base de datos"+"-----------");

except Category.DoesNotExist:
	gangas=Category.objects.get_or_create(catName=prodName)[0]
	gangas.save()
	print("-----------"+prodName+" creadas correctamente"+"-----------");

prodName="oferta 1"
img="oferta.jpg"
description="ofertaa 1"
price=1
stock=11
availability=True


p = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");

prodName="oferta 2"
description="ofertaa 2"
price=2
stock=22
availability=True

p = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");

prodName="oferta 3"
description="ofertaa 3"
price=3
stock=33
availability=True

p = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");	

prodName="ganga 1"
img="oferta.jpg"
description="ganga 1"
price=1
stock=21
availability=True


p = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");

prodName="ganga 2"
description="ganga 2"
price=2
stock=22
availability=True

p = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");

prodName="ganga 3"
description="ganga 3"
price=3
stock=33
availability=True

p = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description,price=price,stock=stock,availability=availability)[0]
imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
p.image.save(img,imageObject, save = True)
p.save()
print("-----------"+prodName+" creadas correctamente"+"-----------");	

prod=Product.objects.filter(category__catName='gangas')
print("Los productos de gangas son:")
for i in prod:
	print ("---"+i.prodName+"---")

aux="oferta-1"
try:
	cat= Category.objects.get(product__prodSlug=aux)#----------------------try catch
	print("\nLa categoria del producto con Slug "+aux+" es: --------"+cat.catSlug+"--------")
except Category.DoesNotExist:
	print("\nLa categoria del producto con Slug " +aux+" no existe")

aux="oferta_10"
try:
	cat= Category.objects.get(product__prodSlug=aux)#----------------------try catch
	print("\nLa categoria del producto con Slug oferta-1 es: --------"+cat.catSlug+"--------")
except Category.DoesNotExist:
	print("\nLa categoria del producto con Slug "+aux+" no existe")

