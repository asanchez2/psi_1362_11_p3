from django import forms
from rango.models import Page, Category

class CategoryForm(forms.ModelForm):
	catName = models.CharField()
	catSlug = models.SlugField()
	

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Category
        fields = ('name',)


class ProductForm(forms.ModelForm):
	category = models.ForeignKey()
	prodName = models.CharField()
	prodSlug = models.SlugField()
	image=models.ImageField()
	description= models.CharField()
	price=models.IntegerField()
	stock=models.BooleanField()
	availability=models.BooleanField()
	created=models.DateTimeField()
	updated=models.DateTimeField()
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Page

        # What fields do we want to include in our form?
        # This way we don't need every field in the model present.
        # Some fields may allow NULL values, so we may not want to include them...
        # Here, we are hiding the foreign key.
        # we can either exclude the category field from the form,
        exclude = ('category',)
        #or specify the fields to include (i.e. not include the category field)
        #fields = ('title', 'url', 'views')

        def clean(self):

            cleaned_data = self.cleaned_data
            url = cleaned_data.get('url')

        # If url is not empty and doesn't start with 'http://', prepend 'http://'.
            if url and not url.startswith('http://'):
                url = 'http://' + url
                cleaned_data['url'] = url

            return cleaned_data
