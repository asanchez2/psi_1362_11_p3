# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify
from datetime import datetime
from django.utils import timezone
# Create your models here.

class Category(models.Model):
	catName = models.CharField(max_length=128, unique=True)
	catSlug = models.SlugField(unique=True)
	
	def save(self, *args, **kwargs):
		self.catSlug = slugify(self.catName)
		super(Category, self).save(*args, **kwargs)

	class Meta:
		verbose_name_plural = 'Categories'
	def __unicode__(self): # For Python 2, use __unicode__ too
		return self.catName

class Product(models.Model):
	category = models.ForeignKey(Category)
	prodName = models.CharField(max_length=128)
	prodSlug = models.SlugField(unique=True)
	image=models.ImageField(upload_to='images/products')
	description= models.CharField(max_length=512)
	price=models.DecimalField(default=1,max_digits=5, decimal_places=2)
	stock=models.IntegerField(default=1)
	availability=models.BooleanField(default=1)
	created=models.DateTimeField(auto_now_add=True)
	updated=models.DateTimeField(auto_now_add=True)

	def save(self, *args, **kwargs):
		self.updated = timezone.now()
		self.prodSlug = slugify(self.prodName)
		super(Product, self).save(*args, **kwargs)


	def __unicode__(self): # For Python 2, use __unicode__ too
		return self.prodName
