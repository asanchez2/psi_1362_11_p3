# -*- coding: utf-8 -*-
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
			'onlineshop.settings')
import django
django.setup()
from shop.models import Category, Product
from django.core.files import File
from django.conf import settings
def populate():
 # First, we will create lists of dictionaries containing the pages
 # we want to add into each category.
 # Then we will create a dictionary of dictionaries for our categories.
 # This might seem a little bit confusing, but it allows us to iterate
 # through each data structure, and add the data to our models.
	pantalones_productos = [
		{"prodName": "pantalon corto rojo",
		"image":"pantalonRojo.jpg",
		"description":"un bonito pantalon rojo, 100 por ciento algodon, fabricado por niños en Indonesia ",
		"price":17.99,
		"stock":23,
		"availability":True},
		{"prodName": "pantalon corto azul",
		"image":"pantalonAzul.jpg",
		"description":"un bonito pantalon azul, 100 por ciento algodon, fabricado por niños en Indonesia ",
		"price":52.99,
		"stock":3,
		"availability":True},
		{"prodName": "pantalon corto negro",
		"image":"pantalonNegro.jpg",
		"description":"un bonito pantalon negro, 100 por ciento algodon, fabricado por niños en Indonesia ",
		"price":17.99,
		"stock":23,
		"availability":True},
		{"prodName":  "pantalon corto gris",
		"image":"pantalonGris.jpg",
		"description":"un bonito pantalon gris, 23 por ciento algodon, fabricado por niños en Africa ",
		"price":1.99,
		"stock":29,
		"availability":True},
		{"prodName":  "pantalon corto verde militar",
		"image":"pantalonVerde.jpg",
		"description":"un bonito pantalon verde militar para tus sesiones de caza humana, 100 por ciento algodon, fabricado por HOMBRES",
		"price":21.99,
		"stock":23,
		"availability":True},
		{"prodName": "pantalon corto blanco",
		"image":"pantalonBlanco.jpg",
		"description":"un bonito pantalon blanco para tu boda, 100 por ciento algodon, fabricado por monos",
		"price":21.99,
		"stock":23,
		"availability":False} ]

	sombreros_productos = [
		{"prodName": "Sombrero Azul",
		"image":"sombreroAzul.jpg",
		"description":"un bonito sombrero azul de fieltro para las tardes de verano",
		"price":17.99,
		"stock":23,
		"availability":True},
		{"prodName": "Sombrero Blanco",
		"image":"sombreroBlanco.jpg",
		"description":"un bonito sombrero blanco de fieltro de fieltro para las tardes de verano",
		"price":24.23,
		"stock":10,
		"availability":True},
		{"prodName": "Sombrero Mejicano",
		"image":"sombreroMejicano.jpg",
		"description":"un gran sombrero mejicano para ser el alma de la fiesta",
		"price":56.32,
		"stock":12,
		"availability":True},
		{"prodName": "Sombrero Negro",
		"image":"sombreroNegro.jpg",
		"description":"un bonito sombrero negro con plumas para las mañana de verano",
		"price":12.99,
		"stock":56,
		"availability":False},
		{"prodName": "Sombrero Paja",
		"image":"sombreroPaja.jpg",
		"description":"un bonito sombrero de paja muy de pueblo",
		"price":15.99,
		"stock":9,
		"availability":True},
		{"prodName": "Sombrero Rojo",
		"image":"sombreroRojo.jpg",
		"description":"un bonito sombrero rojo de edicion limitada, solo existen 3 en el mundo",
		"price":120.99,
		"stock":3,
		"availability":True} ]
	calzoncillos_productos = [
		{"prodName": "Pack de tres calzoncillos grises",
		"image":"calzoncillosPack3gris.jpg",
		"description":"unos bonitos calzoncillos grises",
		"price":17.99,
		"stock":23,
		"availability":True},
		{"prodName": "Pack de tres calzoncillos grises y rojos",
		"image":"calzoncillosPack3rojo.jpg",
		"description":"dos bonitos calzoncillos grises y uno rojo ",
		"price":11.99,
		"stock":20,
		"availability":False},
		{"prodName": "Calzoncillos con zorritos",
		"image":"calzoncilloszorrito.jpg",
		"description":"unos bonitos calzoncillos con dibujos de zorritos para impresionar a cualquiera",
		"price":17.99,
		"stock":123,
		"availability":True},
		{"prodName": "Pack de tres calzoncillos azules y negros",
		"image":"calzoncillosPack3azules.jpg",
		"description":"unos bonitos calzoncillos azules y negros a prueba de manchas",
		"price":11.99,
		"stock":21,
		"availability":True},
		{"prodName": "Pack de tres calzoncillos negros",
		"image":"calzoncillosPack3.jpg",
		"description":"calzoncillos negrgos a prueba de manchas",
		"price":17.99,
		"stock":23,
		"availability":True},
		{"prodName": "Calzoncillo Rojo",
		"image":"pantalonRojo.jpg",
		"description":"un unico y poderoso calzoncillo rojo",
		"price":1.99,
		"stock":3,
		"availability":False} ]		


	cats = {"Pantalones": {"productos": pantalones_productos},
			"Sombreros":{"productos": sombreros_productos},
			"Calzoncillos":{"productos":calzoncillos_productos}}


# If you want to add more catergories or pages,
# add them to the dictionaries above.
# The code below goes through the cats dictionary, then adds each category,
# and then adds all the associated pages for that category.
# if you are using Python 2.x then use cats.iteritems() see
# http://docs.quantifiedcode.com/python-anti-patterns/readability/
# for more information about how to iterate over a dictionary properly.
	for cat, cat_data in cats.items():
		c = add_cat(cat)
		for p in cat_data["productos"]:
			add_product(c, p["prodName"], p["image"], p["description"], p["price"], p["stock"], p["availability"])
	# Print out the categories we have added.
	for c in Category.objects.all():
		for p in Product.objects.filter(category=c):
			print("- {0} - {1}".format(str(c), str(p)))
def add_product(cat, name, img, desc,price,stock,aval):
	p = Product.objects.get_or_create(category=cat, prodName=name, description=desc,price=price,stock=stock,availability=aval)[0]
	imageObject = File(open(os.path.join(settings.BASE_DIR,"imagenes", img),'r'))
	p.image.save(img,imageObject, save = True)
	p.save()
	return p
def add_cat(name):
	c = Category.objects.get_or_create(catName=name)[0]
	c.save()
	return c
# Start execution here!
if __name__ == '__main__':
	print("Starting OnlineShop population script...")
	populate()
